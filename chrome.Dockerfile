FROM ubuntu:20.04

ARG DEBIAN_FRONTEND=noninteractive
ARG TZ=Etc/UTC

RUN apt-get update \
    && apt-get install -y software-properties-common \
    && add-apt-repository -y ppa:savoury1/ffmpeg4 \
    && apt-get install -y \
    curl \
    vim \
    wget \
    unzip \
    gnupg \
    git \
    python3-pip \
    net-tools \
    openjdk-11-jdk-headless \
    ffmpeg \
    xvfb \
    fonts-arphic-ukai \
    fonts-arphic-uming \
    fonts-unfonts-core \
    fonts-ipafont-mincho \
    fonts-ipafont-gothic \
    && apt-get -qyy clean \
    && rm -rf /var/lib/apt/lists/* /var/cache/apt/*

RUN curl -LOf https://archive.apache.org/dist/jmeter/binaries/apache-jmeter-5.5.zip \
    && mkdir -p /opt \
    && mv apache-jmeter-5.5.zip /opt/ \
    && cd /opt && unzip apache-jmeter-5.5.zip
ENV JMETER_HOME=/opt/apache-jmeter-5.5
# Google Chrome Installation
ARG CHROME_VERSION="google-chrome-beta"
RUN mkdir chromeBrowser \
    && cd chromeBrowser \
    && wget https://chrome-versions.com/google-chrome-stable-90.0.4430.72-1.deb \
    && apt-get update \
    && apt-get install -y ./*google-chrome*.deb \
    && rm *google-chrome*.deb \
    && apt-get -qyy clean \
    && rm -rf /var/lib/apt/lists/* /var/cache/apt/*

ENV JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64/
ENV PATH=$PATH:$JAVA_HOME/bin
ENV platform ""
ENV envName ""
ENV parallelThreadCount ""

