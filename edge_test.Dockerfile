FROM ubuntu:20.04

ARG DEBIAN_FRONTEND=noninteractive
ARG TZ=Etc/UTC
#COPY sources.list /etc/apt/sources.list
RUN apt-get update \
    && apt-get install -y software-properties-common \
    && add-apt-repository ppa:savoury1/ffmpeg4 \
    && apt-get install -y \
    curl \
    vim \
    wget \
    unzip \
    gnupg \
    git \
    python3-pip \
    net-tools \
    openjdk-8-jdk

RUN apt-get install -y \
    ffmpeg \
    xvfb \
    fonts-arphic-ukai \
    fonts-arphic-uming \
    fonts-unfonts-core \
    fonts-ipafont-mincho \
    fonts-ipafont-gothic

RUN apt-get -qyy clean \
    && rm -rf /var/lib/apt/lists/* /var/cache/apt/*

# Microsoft Edge Installation
ARG EDGE_VERSION="microsoft-edge-stable=103.0.1264.51-1"
# ARG EDGE_VERSION="microsoft-edge-stable=95.0.1020.53-1"
RUN wget -q -O - https://packages.microsoft.com/keys/microsoft.asc | apt-key add - \
  && echo "deb https://packages.microsoft.com/repos/edge stable main" >> /etc/apt/sources.list.d/microsoft-edge.list \
  && apt-get update -qqy \
  && apt-get -qqy install ${EDGE_VERSION} \
  && rm /etc/apt/sources.list.d/microsoft-edge.list \
  && rm -rf /var/lib/apt/lists/* /var/cache/apt/*

ENV JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/
ENV PATH=$PATH:$JAVA_HOME/bin
ENV platform ""
ENV envName ""
ENV parallelThreadCount ""

ENV MSEDGEDRIVER_TELEMETRY_OPTOUT=1
